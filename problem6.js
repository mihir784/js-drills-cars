function findBmwAndAudis(inventory) {
  let bmwAndAudi = [];

  for (let i = 0; i < inventory.length; i++) {
    if (
      inventory[i]["car_make"] == "BMW" ||
      inventory[i]["car_make"] == "Audi"
    ) {
      bmwAndAudi.push(inventory[i]);
    }
  }

  return bmwAndAudi;
}

export default findBmwAndAudis;
