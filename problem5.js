import findCarYears from "./problem4.js";

function findOldCars(inventory) {
  let years = findCarYears(inventory);
  let oldCars = [];

  for (let i = 0; i < years.length; i++) {
    if (years[i] < 2000) {
      oldCars.push(inventory[i]);
    }
  }

  return oldCars;
}

export default findOldCars;
