function findCarByID(inventory, id = 33) {
  for (let i = 0; i < inventory.length; i++) {
    let car = inventory[i];

    if (car["id"] == id) {
      return (
        `Car ${id} is a ${car["car_year"]} ` +
        `${car["car_make"]} ${car["car_model"]}.`
      );
    }
  }

  return `Car ${id} not found in the inventory.`;
}

export default findCarByID;
