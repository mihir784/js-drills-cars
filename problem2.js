function findLastCar(inventory) {
  let lastIndex = inventory.length - 1;

  if (lastIndex < 0) {
    return `Inventory is empty.`;
  }

  let lastCar = inventory[lastIndex];
  return `Last car is a ${lastCar["car_make"]} ${lastCar["car_model"]}.`;
}

export default findLastCar;
