function merge(left, right) {
  let arr = [];

  while (left.length && right.length) {
    let leftCarName = left[0]["car_model"].toLowerCase();
    let rightCarName = right[0]["car_model"].toLowerCase();

    if (leftCarName < rightCarName) {
      arr.push(left.shift());
    } else {
      arr.push(right.shift());
    }
  }

  return [...arr, ...left, ...right];
}

function mergeSort(arr) {
  const half = arr.length / 2;

  if (arr.length < 2) {
    return arr;
  }

  const left = arr.splice(0, half);
  return merge(mergeSort(left), mergeSort(arr));
}

function sortedInventory(inventory) {
  return mergeSort(inventory);
}

export default sortedInventory;
