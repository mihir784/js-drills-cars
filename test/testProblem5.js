import findOldCars from "../problem5.js";
import inventory from "../inventory.js";

const result5 = findOldCars(inventory);
console.log(result5);
console.log(`${result5.length} cars are older than year 2000.`);
